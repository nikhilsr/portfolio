#include <iostream>
#include <algorithm>
#include <map>


bool compare(char a, char b)
{
    return a>b;
}

int main() {

		int n;
    std::cin >> n;
    for(int i=0;i<n;i++)
    {
        
        string s;
        std::cin >> s;
        int len = s.length();
        std::string sortedS = s;
        sort(sortedS.begin(),sortedS.end());
        std::map<char,int> charMap;
        std::map<int,char> revCharMap;
        int maxInt=0;
        for(int j=0;j<len;j++)
        {
            if(charMap.find(sortedS[j])==charMap.end())
            {
                charMap[sortedS[j]] = maxInt;
                revCharMap[maxInt] = sortedS[j];
                maxInt++;
            }
        }
        std::string newS = s;
        for(int j=len-1;j>=0;)
        {
            std::string::iterator it=newS.end();
            int l=1;

            while(j>0 && ((charMap[newS[j-1]]+l)<charMap.size()) && (it=find(newS.begin()+j,newS.end(),revCharMap[charMap[newS[j-1]]+l]))==newS.end())
            {
                l++;
            }
            if(it==newS.end())
            {
                j--;
            }
            else
            {
                iter_swap(it,newS.begin()+j-1);
                sort(newS.begin()+j,newS.end());
                break;
            }
        }
        
        if(s!=newS)
        {
            std::cout << newS << std::endl;
        }
        else
            std::cout << "no answer" << std::endl;
    }
    return 0;
}

